package com.dummy.notification

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.toast
import com.dummy.notification.utils.F
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener {
            toast("Sending notification in 5 seconds")
            F.scheduleNotification(this, F.getNotification(this, "This is a notification delayed 5 seconds."), 5000)
        }

    }
}
