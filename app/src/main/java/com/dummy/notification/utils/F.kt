package com.dummy.notification.utils

import android.app.AlarmManager
import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.SystemClock
import com.dummy.notification.R
import com.dummy.notification.utils.Publisher


/**
 * @info -
 *
 * @author - Saksham
 * @note Last Branch Update - master
 *
 * @note Created on 2018-10-12 by Saksham
 * @note Updates :
 */
object F {

    fun scheduleNotification(context: Context, notification: Notification, delay: Int) {

        val notificationIntent = Intent(context, Publisher::class.java)
        notificationIntent.putExtra(Publisher.NOTIFICATION_ID, 1)
        notificationIntent.putExtra(Publisher.NOTIFICATION, notification)
        val pendingIntent = PendingIntent.getBroadcast(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val futureInMillis = SystemClock.elapsedRealtime() + delay
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager?
        alarmManager!!.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent)
    }

    fun getNotification(context: Context, content: String): Notification {
        val builder = Notification.Builder(context)
        builder.setContentTitle("Scheduled Notification")
        builder.setContentText(content)
        builder.setSmallIcon(R.mipmap.ic_launcher)
        return builder.build()
    }

}